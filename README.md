# Web前端编码规范

## 编码规范是一种群体智慧的产物

#从 0 到 1 搭建一个企业级前端开发规范  https://mp.weixin.qq.com/s/0-mfeSv-JVeQM_CAXeiLMQ
# Commit Message 规范
大量的代码提交，必然会产生大量的 Commit log. 每一条都 Commit log 记录着某一阶段所完成的事以及关注点，应该尽可能详细具体；在工作中一份清晰规范的 Commit Message 能让后续代码审查、信息查找、版本回退都更加高效可靠。

Commit message 格式
<type>: <subject> 注意冒号后面有空格。

type
用于说明 commit 的类别，只允许使用下面 7 个标识。

feat：新功能（feature）
fix：修补 bug
docs：文档（documentation）
style：格式（不影响代码运行的变动）
refactor：重构（即不是新增功能，也不是修改 bug 的代码变动）
test：增加测试
chore：构建过程或辅助工具的变动
如果 type 为 feat 和 fix，则该 commit 将肯定出现在 Change log 之中。

subject
subject 是 commit 目的的简短描述，不超过 50 个字符，且结尾不加句号（.）。

栗子
git commit -m 'feat:添加了一个用户列表页面'

git commit -m 'fix:修复用户列表页面无法显示的bug'

git commit -m 'refactor:用户列表页面代码重构'

